# Parabyte Securet



## Usage
This software is a graphical user-side software whose task is to manage users connected to your computer with SSH protocol. This software is currently in beta version.
In line with the progress and development of the software and due to the programming interest in open source projects, this software was released under the GPL3 license.

## Technical Specifications
This software is programmed in ANSI C language and by GTK3 graphic library.
You can install this project on your computer by gcc compiler.

## Prerequisites
- gtk3 library
- libnotify library

## Preparation for installation
First, we need to install the gtk3 and libnotify library. You can use the following commands to install this library:

For Debian-based Distribution:
```
sudo apt update
sudo apt install libgtk-3-dev
sudo apt install libnotify-dev
```

For Redhat-based Distribution:
```
sudo dnf update
sudo dnf install gtk3-devel
sudo dnf install libnotify-devel
```

## Start installation
to start installation, insert `sudo make` command in source code directory, and press your favorite Optimization level, like that :
```
$ pwd
/parabyte securet

$ sudo make
i: information..................(note)

0: Standard           compiling (danger)
1: Normal             compiling (not recommended)
2: Slightly optimized compiling
3: optimized          compiling
4: very optimized     compiling
5: super optimized    compiling

Enter compile mode [3]: 3
create binary file to  : [ Ok ]
Create and copy databass and etc [ Ok ]                                                     
```

## Support
To talk and chat with programming, send a message to `libre.hosseini@Gmail.com` or via Matrix protocol to `@librehosseini:matrix.org`

## Roadmap
If you are unemployed and want to do a project, this is good
It has a lot of errors and bugs that need to be debugged


## Authors and acknowledgment
No one in particular helped me with this project. Thanks to the Google search engine :)

## License
This small project is published with a very big license and that license is nothing but GPL 3
I say frankly that you can use this project wherever you want and make any changes in it, except the programmer's name, which please leave it in the corner of the software!

## Project status
This project is under development and programming is doing a lot of effort to increase the quality of the project. Programming is alone and has no colleagues or friends to help it in software development. If you are polite and patient, I will be happy to cooperate with me. I'm a programmer with a lot of cool ideas
