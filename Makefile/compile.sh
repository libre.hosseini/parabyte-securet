#!/bin/bash
sudo clear
printf "i: information..................(note)\n\n"
printf "0: Standard           compiling (danger)\n"
printf "1: Normal             compiling (not recommended)\n"
printf "2: Slightly optimized compiling\n"
printf "3: optimized          compiling\n"
printf "4: very optimized     compiling\n"
printf "5: super optimized    compiling\n\n"

printf "Enter compile mode [3]: "
read choose
if [ "$choose" == "" ]; then
	choose=3
fi
printf "create binary file to $directory : "
if   [ "$choose" == "0" ]; then
  sudo gcc `pkg-config gtk+-3.0 --cflags` Setup.c -o /usr/bin/securet `pkg-config gtk+-3.0 libnotify --libs`

elif [ "$choose" == "1" ]; then
  sudo gcc `pkg-config gtk+-3.0 --cflags` Setup.c -o /usr/bin/securet -Wl,-z,noexecstack     `pkg-config gtk+-3.0 libnotify --libs`

elif [ "$choose" == "2" ]; then
  sudo gcc `pkg-config gtk+-3.0 --cflags` Setup.c -o /usr/bin/securet -Wl,-z,noexecstack -O1 `pkg-config gtk+-3.0 libnotify --libs`

elif [ "$choose" == "3" ]; then
  sudo gcc `pkg-config gtk+-3.0 --cflags` Setup.c -o /usr/bin/securet -Wl,-z,noexecstack -O2 `pkg-config gtk+-3.0 libnotify --libs`

elif [ "$choose" == "4" ]; then
  sudo gcc `pkg-config gtk+-3.0 --cflags` Setup.c -o /usr/bin/securet -Wl,-z,noexecstack -O3 `pkg-config gtk+-3.0 libnotify --libs`

elif [ "$choose" == "5" ]; then
  sudo gcc `pkg-config gtk+-3.0 --cflags` Setup.c -o /usr/bin/securet -Wl,-z,noexecstack -O3 -march=native `pkg-config gtk+-3.0 libnotify --libs`
else
	printf "[ Error ]\n"
	exit
fi
printf "[ Ok ]\n"
printf "Create and copy databass and etc "
sudo rm -r /usr/share/securet-data/ > /dev/null
sudo rm -r ~/.config/securet/ > /dev/null

sudo cp -R ./files /usr/share/securet-data/
mkdir ~/.config/securet/
cp -R ./databass ~/.config/securet/databass
printf "[ Ok ]\n"


# Parabyte Securet is free software: you can redistribute
# it provided that the terms of the GPL 3 license are not
# violated. Parabyte Securet is distributed in the hope
# that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. You should have
# received a copy of the GNU General Public License along
# with Parabyte Securet.
# if not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
# Copyleft (c) Mahdi Hosseini asad, All rights reserved
